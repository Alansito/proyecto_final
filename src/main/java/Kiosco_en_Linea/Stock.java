package Kiosco_en_Linea;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Scanner;

public class Stock {
    String n_product;
    double precio;
    boolean ilegal;
    String decision;
    List<String> nombre_productos = new ArrayList<>();
    List<String> productos_ilegales = new ArrayList<>();
    HashMap<String, Double> precios_productos = new HashMap<>();
    Scanner scanner = new Scanner(System.in);

    public void add_product() {
        System.out.println("Nombre de producto: ");
        n_product = scanner.next();
        nombre_productos.add(n_product);
        System.out.println("Ingrese precio");
        precio = scanner.nextDouble();
        precios_productos.put(n_product, Double.valueOf("$" +precio));
        System.out.println("Si el producto es para mayores de edad ingrese 1, de lo contrario ingrese 2");
        int decision2 = scanner.nextInt();
        switch(decision2){
            case 1:
                productos_ilegales.add(n_product);
                break;
            case 2:
                break;
        }
    }
    public void rm_product(){
        System.out.println("Escriba el nombre del producto a eliminar");
        System.out.println(nombre_productos);
        decision=scanner.next();
        nombre_productos.remove(decision);
        precios_productos.remove(decision);
        if(nombre_productos.contains(decision)){
            System.out.println("ERROR");
        }else{
            System.out.println("Producto eliminado correctamente");
        }
    }
    public void mostrar_lista(){
        for(int i=0 ; i<nombre_productos.size(); i++){
            System.out.println(nombre_productos.get(i));
        }
    }
}
