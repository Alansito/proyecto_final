package Kiosco_en_Linea;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class Cliente {
    String nombre;
    int edad;
    String direccion;
    List<String> lista_clientes = new ArrayList<>();
    Scanner scanner = new Scanner(System.in);


    public void datos (){
        System.out.println("Ingrese su nombre");
        nombre=scanner.next();
        System.out.println("Ingrese su edad");
        edad=scanner.nextInt();
        while(edad<0) {
            System.out.println("Edad invalida");
            System.out.println("Ingrese nuevamente...");
            edad = scanner.nextInt();
        }
        System.out.println("Ingrese su direccion");
        direccion=scanner.next();
    }

    public boolean afiliacion(){
        return true;
    }
}
